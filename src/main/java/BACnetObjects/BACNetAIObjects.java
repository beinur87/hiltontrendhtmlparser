package BACnetObjects;

import TrendObjects.TrendAI;
import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.exception.BACnetServiceException;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.type.constructed.StatusFlags;
import com.serotonin.bacnet4j.type.constructed.ValueSource;
import com.serotonin.bacnet4j.type.enumerated.EventState;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.Polarity;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.ObjectIdentifier;
import com.serotonin.bacnet4j.type.primitive.Real;
import csvprinter.CSVPrinter;

import java.awt.color.ICC_ColorSpace;
import java.util.ArrayList;
import java.util.List;

public class BACNetAIObjects {
    private LocalDevice localDevice;
    private List<TrendAI> rowDataList;

    public BACNetAIObjects(LocalDevice localDevice, List<TrendAI> rowDataList) throws BACnetServiceException {
        this.localDevice = localDevice;
        this.rowDataList = rowDataList;
        createObjects();
    }

    public void createObjects() throws BACnetServiceException {
        int instanceCounter = 1;
        for (TrendAI ai : rowDataList) {
            // Create a test analog value object
            ObjectIdentifier objectId = new ObjectIdentifier(ObjectType.analogValue, instanceCounter);
            BACnetObject object = new BACnetObject(localDevice, objectId);
            ValueSource valueSource = new ValueSource();
            //  localDevice.addObject(object);

// Set some common analog value properties
            object.writeProperty(valueSource, PropertyIdentifier.objectName, new CharacterString(ai.getTag()));
            object.writeProperty(valueSource, PropertyIdentifier.description, new CharacterString(ai.getLabel() + " on outstation " + ai.getOutstation() + " wired to " + ai.getItem()));
            object.writeProperty(valueSource, PropertyIdentifier.presentValue, new Real(Float.parseFloat(ai.getValue())));
            if (ai.getAlarm().equals("Out of Limits")) {
                object.writeProperty(valueSource, PropertyIdentifier.statusFlags, new StatusFlags(true, false, false, false));
                object.writeProperty(valueSource, PropertyIdentifier.eventState, EventState.offnormal);
            } else {
                object.writeProperty(valueSource, PropertyIdentifier.statusFlags, new StatusFlags(false, false, false, false));
                object.writeProperty(valueSource, PropertyIdentifier.eventState, EventState.normal);
            }
            object.writeProperty(valueSource, PropertyIdentifier.outOfService, com.serotonin.bacnet4j.type.primitive.Boolean.valueOf(false));
            // object.writeProperty(valueSource, PropertyIdentifier.deviceType,new CharacterString(ai.getLabel()));


            localDevice.addObject(object);
            instanceCounter++;
        }
    }

    public void updateObjects(List<TrendAI> dataList) throws BACnetServiceException {
        List<String> changesLogger = new ArrayList<>();
        String logLine;
        for (BACnetObject obj : localDevice.getLocalObjects()) {
            if (obj.getObjectName().contains("_")) {
                String objName = obj.getObjectName();
                for (TrendAI ai : dataList) {
                    if (ai.getTag().equals(objName)) {
                        Real trendAIvalue = new Real(Float.parseFloat(ai.getValue()));
                        if (!trendAIvalue.equals(obj.readProperty(PropertyIdentifier.presentValue))) {
                            logLine = "Updated os=" + ai.getOutstation() + " item=" + ai.getItem() + " from "
                                    + obj.readProperty(PropertyIdentifier.presentValue) + " with " + trendAIvalue;
                            changesLogger.add(logLine);
                            obj.writeProperty(new ValueSource(), PropertyIdentifier.presentValue, trendAIvalue);
                        }
                        break;
                    }
                }
            }
        }
        CSVPrinter.writeArrayListToCSV(changesLogger,"updatedAI.txt");


    }


}
