package TrendObjects;

import java.util.ArrayList;
import java.util.List;

public class IPList {

    private List<Integer> ipList = List.of(111,113,114,116,117,119,120,121);

    public IPList() {
    }

    public List<Integer> getIpList() {
        return ipList;
    }

    public void setIpList(List<Integer> ipList) {
        this.ipList = ipList;
    }
}
