package TrendObjects.Parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NavigationButton {
    public String extractSecondAreaHref(String input) {
        String pattern = "<map[^>]*>.*?<area[^>]*href=\"([^\"]+)\".*?>.*?<area[^>]*href=\"([^\"]+)\"";
        Pattern hrefPattern = Pattern.compile(pattern);
        Matcher matcher = hrefPattern.matcher(input);

        if (matcher.find() && matcher.groupCount() >= 2) {
            return matcher.group(2);
        } else {
            return null; // or throw an exception if the pattern is not found
        }
    }

    public String getNextPageLink(String url){
        String nextPageLink = "";
        try {
            Document document = Jsoup.connect(url).get();
            Element mapElement = document.selectFirst("map[name=m_forwardbutton]");
            if (mapElement != null) {
                nextPageLink = extractSecondAreaHref(String.valueOf(mapElement));
                System.out.println("Found <map> element with name 'm_forwardbutton' for URL: " + url);
                return nextPageLink;
            } else {
                System.out.println("No <map> element with name 'm_forwardbutton' found for URL: " + url);
                return null;
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }





}
