package TrendObjects.Parser;

import TrendObjects.IPList;
import TrendObjects.TrendAI;
import TrendObjects.tags.Tag;
import com.sun.source.tree.WhileLoopTree;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileWriter;
import java.io.IOException;
import java.net.ConnectException;
import java.nio.channels.ScatteringByteChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TrendParserAI {
    private List<TrendAI> rowDataList;
    private List<Tag> tags;

    public TrendParserAI(List<Tag> tags) {
        this.tags = tags;
    }

    public List<TrendAI> scan() {
        rowDataList = new ArrayList<>();
        IPList ipList = new IPList();
        for (Integer ip : ipList.getIpList()) {
            String url = "http://192.168.254." + ip + "/S.htm?ovrideStart=0&amp";
            String nextPage = "";
            NavigationButton fwbutton = new NavigationButton();
            while (!nextPage.contains(".htm?ovrideStart=0&")) {
                if (nextPage != null) {
                    parseHTMLTable(url);
                }
                try {
                    nextPage = fwbutton.getNextPageLink(url);
                    url = "http://192.168.254." + ip + "/" + nextPage;
                } catch (NullPointerException e) {
                    System.out.printf("\n");
                    System.out.println("Failed to read page for " + url);
                    System.out.printf("\n");
                }

            }
        }
        writeCSV();
        return rowDataList;
    }


    public void writeCSV() {
        try {
            FileWriter writer = new FileWriter("ai.csv");
            writer.write("Outstation,Item,Tag, Label,Value,Units,Alarm,\n");
            for (TrendAI rowData : rowDataList) {
                writer.write(rowData.getOutstation() + "," + rowData.getItem() + "," + rowData.getTag() + "," + rowData.getLabel() + "," + rowData.getValue() + "," + rowData.getUnits() + "," + rowData.getAlarm() + "\n");
            }
            writer.close();
            System.out.println("CSV file written successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void parseHTMLTable(String url) {
        // List<AlarmTableRowData> rowDataList = new ArrayList<>();
        Document doc = null;
        try {
            doc = Jsoup.connect(url).get();
            Element table = doc.select("td[id=maindata]").first();
            // Element table = doc.select("table").first();
            Elements rows = table.select("tr");
            if (rows.size() >= 9) {
                for (int i = 1; i < rows.size(); i++) {
                    Element row = rows.get(i);
                    Elements cells = row.select("td");
                    String outstation = String.valueOf(Integer.valueOf(extractIpAddress(url)) - 100);
                    try {
                        String item = cells.get(0).text();
                        String label = cells.get(1).text();
                        String value = cells.get(2).text();
                        String units = cells.get(3).text();
                        String alarm = cells.get(4).text();
                        String tag = getTagForIO(outstation, item);
                        if (tag != null) {
                            rowDataList.add(new TrendAI(outstation, item, tag, label, value, units, alarm));
                        }
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("Index out of bounds for " + url + "on row " + i);
                    }
                }
            } else {
                for (int i = 1; i < rows.size() - 1; i++) {
                    Element row = rows.get(i);
                    Elements cells = row.select("td");
                    String outstation = String.valueOf(Integer.valueOf(extractIpAddress(url)) - 100);
                    try {
                        String item = cells.get(0).text();
                        String label = cells.get(1).text();
                        String value = cells.get(2).text();
                        String units = cells.get(3).text();
                        String alarm = cells.get(4).text();
                        String tag = getTagForIO(outstation, item);
                        if (tag != null) {
                            rowDataList.add(new TrendAI(outstation, item, tag, label, value, units, alarm));
                        }
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("Index out of bounds for " + url);
                    }

                }
            }

        } catch (IOException e) {
            System.out.println("Could not access page: " + url);
        }
    }


    public String getTagForIO(String outstation, String item) {
        for (Tag tag : tags) {
            if (tag.getOutstation().equals(outstation) && tag.getItem().equals(item)) {
                return tag.getTagName();
            }
        }
        return null;
    }

    public String extractIpAddress(String url) {
        String regex = "\\b(?:\\d{1,3}\\.){3}\\d{1,3}\\b";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(url);
        if (matcher.find()) {
            String s = matcher.group();
            s = s.substring(s.lastIndexOf('.') + 1);
            return s;
        }
        return null; // no IP address found
    }


}
