package TrendObjects.tags;

public class Tag {
    private String outstation;
    private String item;
    private String tagName;

    public Tag(String outstation, String item, String tagName) {
        this.outstation = outstation;
        this.item = item;
        this.tagName = tagName;
    }

    public String getOutstation() {
        return outstation;
    }

    public void setOutstation(String outstation) {
        this.outstation = outstation;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
