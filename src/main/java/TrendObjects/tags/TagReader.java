package TrendObjects.tags;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TagReader {

    public List<Tag> generateTagsFromCSV(String csvFile) {
        List<Tag> tags = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                Tag tag = new Tag(values[0].trim(),values[1].trim(),values[2].trim());

                // Add the generated tag to the list
                tags.add(tag);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tags;
    }
}
