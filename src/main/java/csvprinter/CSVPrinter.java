package csvprinter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVPrinter {
    public static void writeArrayListToCSV(List<String> arrayList, String filePath) {
        try (FileWriter writer = new FileWriter(filePath)) {
            for (String value : arrayList) {
                writer.append(value);
                writer.append(",");
                writer.append("\n");
            }
            writer.flush();
            System.out.println("ArrayList values written to CSV file successfully.");
        } catch (IOException e) {
            System.out.println("Error writing ArrayList values to CSV file: " + e.getMessage());
        }
    }
}
