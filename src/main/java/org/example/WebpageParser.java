package org.example;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebpageParser {

    public static String extractSecondAreaHref(String input) {
        String pattern = "<map[^>]*>.*?<area[^>]*href=\"([^\"]+)\".*?>.*?<area[^>]*href=\"([^\"]+)\"";
        Pattern hrefPattern = Pattern.compile(pattern);
        Matcher matcher = hrefPattern.matcher(input);

        if (matcher.find() && matcher.groupCount() >= 2) {
            return matcher.group(2);
        } else {
            return null; // or throw an exception if the pattern is not found
        }
    }


    public static void main(String[] args) {
        String url = "http://192.168.254.111/S.htm?ovrideStart=0&";
        String nextPageLink = "";
        while(!nextPageLink.contains(".htm?ovrideStart=0&")) {
            try {
                Document document = Jsoup.connect(url).get();
                Element mapElement = document.selectFirst("map[name=m_forwardbutton]");
                if (mapElement != null) {
                    nextPageLink = extractSecondAreaHref(String.valueOf(mapElement));
                    System.out.println("Found <map> element with name 'm_forwardbutton':");
                    System.out.println(nextPageLink);
                    url = "http://192.168.254.111/"+nextPageLink;
                } else {
                    System.out.println("No <map> element with name 'm_forwardbutton' found.");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
